#!/bin/sh

FREE_MEMORY_KB=$(grep ^MemFree < /proc/meminfo | awk '{print $2}')
echo "TotalFreeMemoryMB = $((FREE_MEMORY_KB / 1024))"

