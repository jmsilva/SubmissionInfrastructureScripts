# Submission Infrastructure Scripts
This repository contains various scripts being used in the submission infrastructure. If you are adding a new script, please do not forget to create a section in the README and explain its usage. 

If you need write access to the repository, please contact the submission infrastructure team at:
cms-submission-infrastructure@cern.ch

# CMSLPCRoute.py
This script works together with the HTCondor job router to create routes for the jobs whitelisting T3_US_FNALLPC. It creates a separate log file (/var/log/condor/CMSLPCRoute.log) detailing its operations. The script behaves as follows:

(1) It fetches the list of LPC users from the CE at FNAL. "CMSLPC_USER_URL" needs to be set in the HTCondor configurations and should point to the location where DN file is hosted.

(2) It then uses the fetched list of user DN to generate a route for the jobs in the queue.